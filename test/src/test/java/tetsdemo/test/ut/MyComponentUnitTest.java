package ut.tetsdemo.test;

import org.junit.Test;
import tetsdemo.test.MyPluginComponent;
import tetsdemo.test.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}